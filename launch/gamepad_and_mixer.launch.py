from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():

    gamepad_controller_node = Node(
            package='input_devices',
            executable='gamepad_controller_node',
            name='gamepad',
            output='screen',
        )
    
    microros_agent_node = Node(
            package="micro_ros_agent",
            executable="micro_ros_agent",
            arguments=["serial", "--dev", "/dev/ttyACM0"],
            name='mixer',
            output="screen",
            )
    
    return LaunchDescription([
        gamepad_controller_node,
        microros_agent_node
    ])
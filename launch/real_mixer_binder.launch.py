from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():

    mixer_controller_node = Node(
            package='input_devices',
            executable='mixer_controller_node',
            name='real_mixer_binder',
            output='screen',
            parameters=[
            {"virtual_mixer": False,
             "demo":False}
        ]
        )
    
    return LaunchDescription([
        mixer_controller_node
    ])
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():

    mixer_controller_node = Node(
            package='input_devices',
            executable='mixer_controller_node',
            name='virtual_mixer_binder',
            output='screen',
            parameters=[
            {"virtual_mixer": True,
             "demo":True}
        ]
        )
    
    return LaunchDescription([
        mixer_controller_node
    ])
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Joy
from .gamepad import Gamepad

class GamepadControllerNode(Node):

    def __init__(self):
        super().__init__('gamepad_controller_node')

        self.gamepad = Gamepad()
    
        # Publisher
        self.gamepad_publisher = self.create_publisher(Joy, '/gamepad', 10)

        # Subscriber
        self.gamepad_subscriber = self.create_timer(0.0001, self.gamepad_callback) 

    def gamepad_callback(self):
        joy_data = Joy()

        joy_data.buttons = list(self.gamepad.button_data.values())
        joy_data.axes = list(self.gamepad.axis_data.values())

        self.gamepad_publisher.publish(joy_data)
        

            
def main(args=None):
    rclpy.init(args=args)

    gamepad_controller_node = GamepadControllerNode()

    rclpy.spin(gamepad_controller_node)

    gamepad_controller_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
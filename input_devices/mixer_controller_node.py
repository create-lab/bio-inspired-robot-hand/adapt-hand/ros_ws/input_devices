import rclpy
from rclpy.node import Node
from std_msgs.msg import String
from rcl_interfaces.msg import ParameterDescriptor
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Int32MultiArray

from .mixer_binder import MixerBinder

class MixerControllerNode(Node):

    def __init__(self):
        super().__init__('mixer_controller_node')

        self.mixer_binder = MixerBinder()

        # Parameters
        param_descriptor = ParameterDescriptor(
            description='Whether or not using the dummy mixer which is to use keyboard commands')
        self.declare_parameter('virtual_mixer', True, param_descriptor)


        param_descriptor = ParameterDescriptor(
            description='Whether doing a demo or not --> add intermediate tendon displacement node')
        self.declare_parameter('demo', True, param_descriptor)

        # Tendon position publisher
        if self.get_parameter('demo').value:
            self.tendon_pos_publisher = self.create_publisher(Float64MultiArray, '/mixer_tendon_position_demand', 10)
        else:
            self.tendon_pos_publisher = self.create_publisher(Float64MultiArray, '/tendon_position_demand', 10)


        # If using a dummy mixer
        if self.get_parameter('virtual_mixer').value:
            self.virtual_mixer_subscriber = self.create_timer(0.001, self.virtual_mixer_callback)

        # If using the real mixer
        else:
            self.mixer_subscriber = self.create_subscription(Int32MultiArray, '/mixer_signal', 
                                                             self.mixer_callback, 10)

    def virtual_mixer_callback(self):
        self.mixer_binder.get_virtual_mixer_input()
        tendon_demand_dict = self.mixer_binder.bind_tendons_to_mixer()

        tendon_pos_demand = Float64MultiArray()
        tendon_pos_demand.data = list(tendon_demand_dict.values())

        self.tendon_pos_publisher.publish(tendon_pos_demand)

    def mixer_callback(self, msg: Int32MultiArray):
        # self.get_logger().info('I heard: "%s"' % "asdfasdf", throttle_duration_sec = 1)
        self.mixer_binder.get_mixer_input(msg.data)
        tendon_demand_dict = self.mixer_binder.bind_tendons_to_mixer()
        # tendon_demand_dict = self.mixer_binder.bind_pose_to_mixer()

        tendon_pos_demand = Float64MultiArray()
        tendon_pos_demand.data = list(tendon_demand_dict.values())

        self.tendon_pos_publisher.publish(tendon_pos_demand)


def main(args=None):
    rclpy.init(args=args)

    mixer_controller_node = MixerControllerNode()

    rclpy.spin(mixer_controller_node)

    mixer_controller_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()